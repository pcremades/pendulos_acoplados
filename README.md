# Pendulos_Acoplados

Sistema diseñado para estudio de osciladores acoplados y/o forzados.

![Render](https://gitlab.com/pcremades/pendulos_acoplados/raw/master/Img/Render.png)

## Materiales
- [Arduino Uno](https://store.arduino.cc/usa/arduino-uno-rev3/)
- [Sensor de efecto Hall Key-024](https://www.tkkrlab.nl/wiki/Arduino_KY-024_Linear_magnetic_Hall_sensors)
- [Eje recuperado de impresora HP o Lexmark](https://gitlab.com/pcremades/pendulos_acoplados/raw/master/Img/Printer.jpeg)
- Varilla roscada de 5mm
- Tornillo de 3mm con tuercas
- Impresora 3D. [Aquí](https://gitlab.com/pcremades/pendulos_acoplados/blob/master/hardware) puede encontrar los archivos STL para imprimir la piezas.